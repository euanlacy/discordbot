import discord
from discord.ext import commands
import requests
import random
import praw
import os
import urbandictionary as ud
import rainbow_team
import rainbow
import asyncio
import aiohttp

# Put the values for the reddit api and the discord bot id in secrets.txt
secrets = open(os.path.dirname(__file__) + '/../secrets.txt', 'r')


keys = secrets.readlines()
keys = [item[:-1] for item in keys]  # remove \n
reddit = praw.Reddit(client_id=keys[0],
                     client_secret=keys[1],
                     user_agent=keys[2])


def getMeme(sub, lim):
    subreddit = reddit.subreddit(sub)
    posts = [post for post in subreddit.hot() if post.score > lim]
    random.shuffle(posts)
    for post in posts:
        url = post.url
        end = url.split('/')[3]
        if '.' in end:
            return (url, post.permalink, post.title, post.score)
        elif 'gfycat' in url:
            r = requests.get("http://gfycat.com/cajax/get/" + end).json()
            if r["gfyItem"]["gifUrl"]:
                return (r["gfyItem"]["gifUrl"], post.permalink,
                        post.title, post.score)
            else:
                return (r["gfyItem"]["webmUrl"],
                        post.permalink, post.title, post.score)


bot = commands.Bot(command_prefix='$')


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.command()
async def greet(ctx):
    await ctx.send(":smiley: :wave: Hello, there!")


@bot.command()
async def yeet(ctx):
    await ctx.send("Yeet")


@bot.command()
async def cat(ctx):
    async with aiohttp.get("http://thecatapi.com/api/images/get?format=src&type=gif") as r:
        embed = discord.Embed(title="Cat", color=0xd20005)
        embed.set_image(url=r.url)
        await ctx.send(embed=embed)


@bot.command()
async def meme(ctx, sub: str = "meirl", lim: int = 50):
    post = getMeme(sub, lim)
    perma = "https://www.reddit.com" + post[1]
    img = post[0]
    score = post[3]
    if any(x in img for x in ["webm", "gifv"]):
        await ctx.send(img)
    else:
        embed = discord.Embed(title="Subreddit: " + sub, url=perma,
                              description=post[2], color=0xd50000)
        embed.set_image(url=img)
        embed.set_footer(text=score)
        await ctx.send(embed=embed)


@bot.command(pass_context=True)
async def test(ctx, role: discord.Role = None, user: discord.Member = None):
    if role not in user.roles:
        print("add role")
        await ctx.add_roles(user, role)
    elif role in user.roles:
        print("remove role")
        await ctx.remove_roles(user, role)
    else:
        print("Fucked up")


@bot.command()
async def r6(ctx, player: str, op: str):
    newData = await rainbow.main(player, op)
    time = str(round((int(newData[1])) / (60 * 60), 2))
    if op == 'player':  # send player stats
        embed = discord.Embed(title="Player Stats", color=0x7a3fbc)
        embed.add_field(name='Time', value=time, inline=True)
        embed.add_field(name='Kills', value=newData[0], inline=True)
        embed.add_field(name='Kills/Death', value=newData[3], inline=True)
        embed.add_field(name='Melees', value=newData[2], inline=True)
        embed.add_field(name='Accuracy', value=newData[4], inline=True)
        embed.add_field(name='Headshots', value=newData[5], inline=True)
        await ctx.send(embed=embed)
    else:  # send operator stats
        embed = discord.Embed(title=op + " Stats", color=0x7a3fbc)
        embed.add_field(name='Time', value=time, inline=True)
        embed.add_field(name='Kills', value=newData[0], inline=True)
        embed.add_field(name='Kills/Death', value=newData[4], inline=True)
        embed.add_field(name='Melees', value=newData[3], inline=True)
        embed.add_field(name='Injures', value=newData[5], inline=True)
        embed.add_field(name='Headshots', value=newData[2], inline=True)
        await ctx.send(embed=embed)


@bot.command()
async def team(ctx, *players):
    numPlayers = len(players)
    if 1 < numPlayers < 11:
        team1, team2, value = await rainbow_team.main(players)
        embed = discord.Embed(title="Teams")
        embed.add_field(name="Team1", value=team1, inline=True)
        embed.add_field(name="Team2", value=team2, inline=True)
        embed.add_field(name="Value", value=value, inline=True)
        await ctx.send(embed=embed)
    else:
        await ctx.send("Enter a valid number of players")


@bot.command()
async def info(ctx):
    embed = discord.Embed(title="Big-Bot",
                          description="Memes, Rainbow 6 stats",
                          color=0xeee657)
    # give info about you here
    embed.add_field(name="Author", value="Euan Lacy")

    # give users a link to invite this bot to their server
    embed.add_field(name="Invite",
                    value="[Invite link](https://discordapp.com/api/oauth2/authorize?client_id=329024187249197056&permissions=8&scope=bot)")

    await ctx.send(embed=embed)


@bot.command()
async def roll(ctx, dice: str):
    """Rolls a dice in NdN format."""
    try:
        rolls, limit = map(int, dice.split('d'))
    except Exception:
        await ctx.send('Format has to be in NdN!')
        return

    result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
    await ctx.send(result)


@bot.command()
async def commands(ctx):
    embed = discord.Embed(title="Big-Bot",
                          description="Commands:",
                          color=0xeee657)

    embed.add_field(name="$greet", value="Gives a nice greet message", inline=False)
    embed.add_field(name="$cat", value="Gives a cute cat gif to lighten up the mood.", inline=False)
    embed.add_field(name="$meme [subreddit] [upvote limit]", value="Takes a random picture from a subreddit",
                    inline=False)
    embed.add_field(name="$r6 [username] [operator or player]", value="Gives info on a player in Rainbow6",
                    inline=False)
    embed.add_field(name="$info", value="Gives a little info about the bot", inline=False)
    embed.add_field(name="$yeet", value="Yeet", inline=False)
    embed.add_field(name="$help", value="Gives this message", inline=False)

    await ctx.send(embed=embed)

bot.run(keys[3])
